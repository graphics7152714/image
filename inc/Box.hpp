/**
 * @file Box.hpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief Simple box with position and size
 * @version 0.1
 * @date 2024-04-14
 *
 * Remarks:
*/
#pragma once

#include <cstdint>

template <typename T = std::uint16_t>
class Box {
public:
  Box(T _Width, T _Height) : Width(_Width), Height(_Height) {}
  Box(T _Left, T _Top, T _Width, T _Height) : Left(_Left), Top(_Top), Width(_Width), Height(_Height) {}

  T Left = 0;
  T Top = 0;
  T Width = 0;
  T Height = 0;
};