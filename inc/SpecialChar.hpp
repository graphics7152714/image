/**
 * @file SpecialChar.hpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Delivers a list of special non-ascii chars
 * @version 1.0
 * @date 2023-11-23
 *
 * Remarks:
*/
#pragma once
#include <cstdint>
#include <array>

namespace Graphics {
  class SpecialChar {
  public:
    SpecialChar(const char* _Key, std::uint8_t _Value);
    const char* Key;
    const std::uint8_t Value;
    const std::uint8_t Key_Length;
  };

  namespace SpecialCharCharacters {
    constexpr std::uint8_t Number = 8;
  }

  extern const std::array<SpecialChar, SpecialCharCharacters::Number> SpecialChars;
};
